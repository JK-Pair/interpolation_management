import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from unicodedata import normalize
import calendar
import argparse
import json
from datetime import date
import srad_calculation
from sqlite_interpolation import *
# ROOT_DIR = os.path.abspath(os.curdir)

class interpolation_input:
    def __init__(self, info_tmd_json, elevation_path):
        '''
        info_tmd_json [str]: csv file path
        '''
        self.info_tmd_json = info_tmd_json
        self.stationID_keys, self.weather_data = self.get_weather_info()
        self.elevation_path = elevation_path
        self.elevation = self.get_elevation_m()
    
    def doy(self, date):
        year = date.split("-")[0] 
        start_date = year + "-01-01"
        dt = pd.to_datetime(start_date, format="%Y-%m-%d")
        dt1 = pd.to_datetime(date, format="%Y-%m-%d")

        delta_date =  "%03d" % int((dt1-dt).days+1)
        result = year + str(delta_date)
        return result

    def readJSON(self, file_path):
        with open(file_path) as file_:
            data = json.load(file_)
        return data

    def get_weather_info(self):
        readjson = self.readJSON(self.info_tmd_json)
        return list(readjson.keys()), readjson

    def get_elevation_m(self):
        elevation = dict()
        for station in self.readJSON(self.elevation_path):
            wmo_code = station["procedure"].split(":")[-1]
            info_dict = dict()
            if wmo_code in self.stationID_keys:
                info_dict["longitude"] = station["location"]["coordinates"][0]
                info_dict["latitude"]  = station["location"]["coordinates"][1]
                info_dict["elevation"] = station["location"]["coordinates"][-1]
                elevation[wmo_code] = info_dict
        return elevation

    def get_srad(self, date, dict_target, sta_id):

        get_info = self.elevation[sta_id]
        latitude = get_info["latitude"]
        longitude = get_info["longitude"]
        eleva = get_info["elevation"]

        actual_sunshine = dict_target["day_length"]
        if np.isnan(actual_sunshine) or float(actual_sunshine) == -99.0:
            dict_target["srad"] = None

        if float(dict_target["max_temperature"]) == -99.0:
            dict_target["max_temperature"]  = None

        if float(dict_target["min_temperature"]) == -99.0:
            dict_target["min_temperature"]  = None

        if float(dict_target["rainfall"]) == -99.0:
            dict_target["rainfall"]  = None

        elif "day_length" in list(dict_target.keys()):
            Rs = srad_calculation.main(date, actual_sunshine, latitude)
            dict_target["srad"] = Rs 
            #Add station location as well.
        
        dict_target["tmax"] = dict_target.pop("max_temperature")
        dict_target["tmin"] = dict_target.pop("min_temperature")
        dict_target["rain"] = dict_target.pop("rainfall")

        dict_target["wmocode"] = sta_id
        dict_target["latitude"] = str(latitude)
        dict_target["longitude"] = str(longitude)
        dict_target["elevation"] = eleva
        dict_target.pop("day_length")
        dict_target.pop("relative_humidity")

        return dict_target

    def get_all_dates(self):
        result_list = list()
        get_station = list(self.weather_data)[0]
        for date, values in self.weather_data[get_station].items():
            result_list.append(date)
        return result_list

    def main(self, target_date):
        #Read TMD"s info from 
        doy = self.doy(target_date)
        result_list = []
        
        for station_id, values in self.weather_data.items():
            weather_data = values[target_date]
            #To calulate SRAD
            try:
                result_list.append(self.get_srad(target_date, weather_data, station_id))
            except :
                continue

        return doy, result_list
    
def dnn_interpolation(date_of_year, raw_data):
    interpolated_data = daily_interpolation(date_of_year, raw_data)
    return interpolated_data
        

if __name__ == "__main__":
    from dnn_interpolation.src.daily_interpolation import daily_interpolation

    tmd_class = interpolation_input("TMD_weather_data_converted.json", "response.json")
    db_path = db_root_path + '\interpolated.db'

    for date_ in tmd_class.get_all_dates():
        print("date ", date_)

        doy, input_df = tmd_class.main(date_)
        filled_df = dnn_interpolation(int(doy[4:]), input_df)

        db_called = sqlite_interpolation(db_path)

        for info in filled_df:
            db_called.main(doy, info)

    
    db_called = sqlite_interpolation(db_path)
    # print(db_called.query_all_data("provinces"))
    print("\n")
    # print(db_called.query_all_data("weather_data"))
    print(db_called.query_data(48431))
