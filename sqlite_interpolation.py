import sqlite3
from sqlite3 import Error

class sqlite_interpolation:

    def __init__(self, db_path):
        self.database_path = db_path
        self.conn_db = self.create_connection()
        

    def create_connection(self):
        """ create a database connection to the SQLite database
            specified by db_file
        :param db_file: database file
        :return: Connection object or None
        """
        conn = None
        try:
            conn = sqlite3.connect(self.database_path)
            return conn
        except Error as e:
            print(e)

        return self.conn_db

    def create_table_tool(self, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = self.conn_db.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)
        return None

    def create_inital_table(self):
        '''
        To create a initial table
        '''
        sql_create_province_table = """ CREATE TABLE IF NOT EXISTS provinces (
                                        station_id integer NOT NULL,
                                        doy text,
                                        PRIMARY KEY (station_id, doy)
                                        ); """

        sql_create_wtd_table = """CREATE TABLE IF NOT EXISTS weather_data (
                                        station_id integer,
                                        DATE text,
                                        TMIN integer NOT NULL,
                                        TMAX integer NOT NULL,
                                        SRAD integer NOT NULL,
                                        RAIN integer NOT NULL,
                                        latitude integer NOT NULL,
                                        longitude integer NOT NULL,
                                        elevation integer NOT NULL,
                                        PRIMARY KEY (station_id, DATE),
                                        FOREIGN KEY (station_id, DATE) REFERENCES provinces (station_id, doy)
                                );"""


        # create tables
        if self.conn_db is not None:
            # create projects table
            self.create_table_tool(sql_create_province_table)
            self.create_table_tool(sql_create_wtd_table)

        else:
            print("Error! cannot create the database connection.")
        return None

    def insert_wtd_tool(self, wtd_data):
        """
        Create a new wtd_data
        :param conn:
        :param task:
        :return:
        """

        sql = ''' INSERT INTO weather_data(
            station_id,
            DATE,
            TMIN,
            TMAX,
            SRAD,
            RAIN,
            latitude,
            longitude,
            elevation)
            VALUES(?,?,?,?,?,?,?,?,?) '''

        cur = self.conn_db.cursor()
        cur.execute(sql, wtd_data)
        self.conn_db.commit()

        return cur.lastrowid

    def inset_province_tool(self, province):
        """
        Create a new province into the province table
        :param conn:
        :param project:
        :return: project id
        """
        sql = ''' INSERT INTO provinces(station_id, doy)
            VALUES(?,?) '''

        cur = self.conn_db.cursor()
        cur.execute(sql, province)
        self.conn_db.commit()
        return cur.lastrowid

    def query_all_data(self, tableName):
        """
        Query all rows in the tasks table
        :param conn: the Connection object
        :return:
        """
        cur = self.conn_db.cursor()
        command = "SELECT * FROM {}".format(tableName)
        cur.execute(command)

        rows = cur.fetchall()

        return rows

    def query_data(self, station_id):
        """
        Query all rows in the tasks table
        :param conn: the Connection object
        :return:
        """
        cur = self.conn_db.cursor()
        command = "SELECT * FROM weather_data WHERE station_id={}".format(station_id)
        cur.execute(command)

        rows = cur.fetchall()

        return rows

    def main(self, doy, data_input):
        '''
        date: '2020001'
        data_input:
        {
            'srad', 
            'tmax', 
            'tmin', 
            'rain', 
            'wmocode', 
            'latitude', 
            'longitude', 
            'elevation'
        }
        '''
        # create a database connection
        self.create_inital_table()

        with self.conn_db:

            wmocode = data_input['wmocode']
            srad = data_input['srad']
            tmax = data_input['tmax']
            tmin = data_input['tmin']
            rain = data_input['rain']
            latitude = data_input['latitude']
            longitude = data_input['longitude']
            elevation = data_input['elevation']

            # Insert data into tables.
            province_data = (wmocode, doy)
            self.inset_province_tool(province_data)

            wtf_data = (wmocode, doy, tmin, tmax, srad, rain, latitude, longitude, elevation)
            self.insert_wtd_tool(wtf_data)


if __name__ == '__main__':
    file_path = r"C:\Users\thipawan\Desktop\workspaces\lf-weather-data-feeder\sqlite"
    df_path = file_path + '\interpolated.db'
    call_class = sqlite_interpolation(df_path)

    test =  {'srad': 19.305191040039062, 'tmax': 30.5, 'tmin': -99.0, 'rain': 0.0, 'wmocode': '48583', 'date': '2020-01-01', 'latitude': '6.416666667', 'longitude': '101.8166667', 'elevation': 8.63}
    test2 =  {'srad': 19.305191040039062, 'tmax': 30.5, 'tmin': -99.0, 'rain': 0.0, 'wmocode': '48583', 'date': '2020-01-02', 'latitude': '6.416666667', 'longitude': '101.8166667', 'elevation': 8.63}
    test3 =  {'srad': 19.305191040039062, 'tmax': 30.5, 'tmin': -99.0, 'rain': 0.0, 'wmocode': '48599', 'date': '2020-01-02', 'latitude': '6.416666667', 'longitude': '101.8166667', 'elevation': 8.63}
    # call_class.main("2020001", test)
    # call_class.main("2020002", test2)
    # call_class.main("2020002", test3)

    # print(call_class.query_all_data("provinces"))
    # print(call_class.query_all_data("weather_data"))
    print(call_class.query_data(48583))



